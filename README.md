LogPrototype
============

Check Point #1:Applet template which will be used as Basic GUI created by Reilly. Uknown what Added by Aaron.
Check Point #2: Most of time between checkpoints was used to familiarize self with the use of GitHub on netBeans.Basic structures built and thought about ; however, as didn't have date class not much added. Aaron Created Date class which will have multiple uses in calculations and file storage.
Check Point #3: Can't find the Date class, Aaron didn't upload properly to GitHub, possibly due to having experience primarily in the Git download and not the Netbeans plugin. Researched File, Folder and Directory to try and find ways to store and organize the User data and entrys. Basic classes added; However, for the most part they are still empty of most of their desired funtions. Most likely going to meet over weekend to organize use of GitHub and get some more substantial work donein terms of functionality.
Final Check in:
Finished design of GUI. Back end data management is complete, Most of login was complete when I gave the project to Aaron to connect the GUI to data and functionality.

Description: Group project with friend making a java prototype for a website we will make at a later date. As we have
very little experience with HTML, CSS, PHP, and SQL we are making a prototype of what we want to make in java
which we both know well.

How to Run: Make sure there is a File called LogProtoTypeData in the C drive. Run the JFrameLogRunner. Create an account( runner or User) and you can add entrys and it will record them and save it in the LogProtoTypeData file. Account data such as username and password are also saved.

Highlights: Reilly focused very strongly on being able to use file saving and organizing the Classes to be flexible in regard to getting the data and working with it. Reilly also created the design for the GUI. Aaron focused on the calculations and then was in charge of attatching functionality to the GUI.

Bugs: Due to the struggle with Aaron being out of town we didn't have time to extensively test for bugs and as such it is likely that there are several.
