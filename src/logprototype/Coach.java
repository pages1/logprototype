/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

/**
 * @author Reilly Nooan Grant & Aaron Spak
 * Creating a prototype in java of the web based application that me and 
 * Aaron Spak plan to build. This is one Users that will represent a
 * runner who will contains a logbook in 
 * addition to a user name and password
 */
public class Coach extends User{
    private Team team;
    
    public Coach(String userName, String password,String teamName){
        super(userName,password,teamName);
        team=new Team(teamName);
    }
    
    public String[] teamMembers(){
        String[] theory = new String[team.getRunners().size()];
        for(int i=0; i<theory.length;i++){
            theory[i]=team.getRunners().get(i).getUserName();
        }
        return theory;
    }
    
}
