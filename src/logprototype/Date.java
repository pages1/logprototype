package logprototype;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Map;
import java.util.TreeMap;


/**
 *date class created by Aaron Spak designed to hold month, year and day
 * @author 151spaka
 */
public class Date {
    private int month;
    private int year;
    private int day;
    private int numDaysInYear;
    //The day of week on 11/3/13 is SUNDAY, aka 0
    //11/3/13 is day 307 of the year
    public final int CURRENT_DAY_CONSTANT =307;
    public final int CURRENT_DAYOFWEEK_CONSTANT=0;
    private int dayOfWeek;//Sunday=0, monday=1, etc.
//    private static final int JANUARY_DAYS = 31;
//    private static final int FEBRUARY_DAYS = 28;
//    private static final int FEBRUARY_DAYS_LEAP_YEAR = 29;
//    private static final int MARCH_DAYS = 31;
//    private static final int APRIL_DAYS = 30;
//    private static final int MAY_DAYS = 31;
//    private static final int JUNE_DAYS = 30;
//    private static final int JULY_DAYS = 31;
//    private static final int AUGUST_DAYS = 31;
//    private static final int SEPTEMBER_DAYS = 30;
//    private static final int OCTOBER_DAYS = 31;
//    private static final int NOVEMBER_DAYS = 30;
//    private static final int DECEMBER_DAYS = 31;
    private Map<Integer,Integer> daysInMonth = new TreeMap<>();
    
    public Date(int m, int d, int y){
        daysInMonth.put(1,31);//January
        daysInMonth.put(2,28);//February
        daysInMonth.put(3,31);//March
        daysInMonth.put(4,30);//April
        daysInMonth.put(5,31);//May
        daysInMonth.put(6,30);
        daysInMonth.put(7,31);
        daysInMonth.put(8,31);
        daysInMonth.put(9,30);
        daysInMonth.put(10,31);
        daysInMonth.put(11,30);
        daysInMonth.put(12,31);//December
        if(y<0) year=2013;
        else year = y;
        switch(m){
            case 4: case 6: case 9: case 11:
                month = m;
                if(d>30||d<1){
                    day = 1;
                }
                else day = d;
                break;
            case 2:
                month = m;
                if((year%4==0&&year%100!=0)||year%400==0){
                    if(d>29||d<1){
                        day=1;
                    }
                    else day = d;
                }
                else{
                    if(d>28||d<1){
                        day=1;
                    }
                    else day = d;
                }
           case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                month = m;
                if(d>31||d<1){
                    day = 1;
                }
                else day = d;
                break;
           default:
               month =1;
               if(d>31||d<1){
                    day = 1;
                }
                else day = d;
                break;
        }
        
        if((year%4==0&&year%100!=0)||year%400==0)
            numDaysInYear=366;
        else
            numDaysInYear=365;
        
    }
    public int getDay(){
        return day;
    }
    public int getMonth(){
        return month;
    }
    public int getYear(){
        return year;
    }
    /**
     * returns an int representing the day of week
     * @return an int representing the day of week
     */
    public int getDayOfWeek(){
        int daySum = 0;
        boolean isLeapYear=false;
        if(year>2013){
            for(int i = 2014; i<year; i++){
                if((i%4==0&&i%100!=0)||i%400==0){
                    isLeapYear=true;
                    daySum+=366;
                }
                else{
                    daySum+=365;}
            }
            if(isLeapYear){
                daySum++;}
            for(int i = 1; i<=month-1; i++){
                daySum+=daysInMonth.get(i);
            }
            daySum+=365-307;
            daySum+=day;
            dayOfWeek=daySum%7;
        }
        else if(year==2013){
            if(month>11){
                dayOfWeek=((334+day)-CURRENT_DAY_CONSTANT)%7;
            }
            else{//month==11
                dayOfWeek=(day-3)%7;
            }
        }
        return dayOfWeek;
    }
    //Not quite Complete, will be fixing tonight
    /**
     * returns the number of days between this date and another date
     * @param d the date to find the number of days between
     * @return an int representing the number of days in-between this date and 
     * another
     */
    public int getNumDaysBetween(Date d){
        int daySum = 0;
        boolean isLeapYear=false;
        if(d.getYear()>year){
            for(int i = year; i<=d.getYear()-1; i++){
                if((i%4==0&&i%100!=0)||i%400==0){
                    isLeapYear=true;
                    daySum+=366;
                }
                else
                    daySum+=365;
            }
            if((d.getYear()%4==0&&d.getYear()%100!=0)||d.getYear()%400==0)
                daySum++;
            for(int i = month; i<=d.getMonth()-1; i++){
                daySum+=daysInMonth.get(i);
            }
            int startDateNumDay = 0;
            for(int i =d.getMonth(); i<=month-1; i++){
                startDateNumDay+=daysInMonth.get(i);
            }
            startDateNumDay+=day;
            daySum+=(365-startDateNumDay);
            daySum+=d.getDay();
        }
        else if(year>d.getYear()){
            for(int i = d.getYear(); i<year-1; i++){
                if((i%4==0&&i%100!=0)||i%400==0){
                    isLeapYear=true;
                    daySum+=366;
                }
                else
                    daySum+=365;
            }
            if(isLeapYear)
                daySum++;
            for(int i = 1; i<=month-1; i++){
                daySum+=daysInMonth.get(i);
            }
            int startDateNumDay = 0;
            for(int i =1; i<=d.getMonth()-1; i++){
                startDateNumDay+=daysInMonth.get(i);
            }
            daySum+=365-startDateNumDay;
            daySum+=day;
        }
        else if(d.getYear()==2013&&year==2013){
            int startDateNumDay = 0;
            if(month>11){
                startDateNumDay=((334+day)-CURRENT_DAY_CONSTANT);
            }
            else{//month==11
                startDateNumDay=(day-3);
            }
            if(d.getMonth()>11){
                daySum=((334+day)-CURRENT_DAY_CONSTANT);
            }
            else{//month==11
                daySum=(day-3);
            }
            daySum=Math.abs(daySum-startDateNumDay);
        }
    return daySum;
    }
    /**
     * return a string representing the date that this date object holds
     * @return a string representing the date that this date object holds.
     */
    public String toString(){
        return month+"_"+day+"_"+year+".";
    }
}
