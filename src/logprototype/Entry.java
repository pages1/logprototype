/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Reilly Nooan Grant & Aaron Spak
 * Creating a prototype in java of the web based application that me and 
 * Aaron Spak plan to build. This is one of the most basic units designed to 
 * store all the information of one entry in a logbook
 * Also, note to self Use PrintWriter to change files and scanner to read.
 * Find out more about folders to be used in logbook
 */
public class Entry {
    private double miles;
    private double sleep;
    private String notes;
    private int hours;
    private int minutes;
    private int seconds;
    private String[] inputs;
    //private Date date; Waiting on Spaks Date class, Propably don't acctually
    //need Entry to have a Date, we can simply store the date in the log book
    //and use a TreeMap for dates
    
    /**
     * Constructs an entry based on a file given as input
     * @param file the file given as input
     * @throws FileNotFoundException 
     */
    public Entry(File file) throws FileNotFoundException{
        Scanner reader = new Scanner(file);
        inputs=new String[6];
        for (int i=0; i<inputs.length;i++){
            inputs[i]= reader.nextLine();
        }
        miles=Double.parseDouble(inputs[0]);
        sleep = Double.parseDouble(inputs[1]);
        notes=inputs[2];
        hours=Integer.parseInt(inputs[3]);
        minutes=Integer.parseInt(inputs[4]);
        seconds=Integer.parseInt(inputs[5]);
    }
    /**
     * Constructs an entry based on a string array given as input
     * @param inputs the string array given as input.
     */
    public Entry(String[] inputs){
        this.inputs=inputs;
        miles=Double.parseDouble(inputs[0]);
        sleep = Double.parseDouble(inputs[1]);
        notes=inputs[2];
        hours=Integer.parseInt(inputs[3]);
        minutes=Integer.parseInt(inputs[4]);
        seconds=Integer.parseInt(inputs[5]);
    }
    /**
     * edits the entry using the string array inputed
     * @param inputs the string input to edit the entry based on.
     */
    public void editAll(String[] inputs){
        this.inputs=inputs;
        miles=Double.parseDouble(inputs[0]);
        sleep = Double.parseDouble(inputs[1]);
        notes=inputs[2];
        hours=Integer.parseInt(inputs[3]);
        minutes=Integer.parseInt(inputs[4]);
        seconds=Integer.parseInt(inputs[5]);
    }
    /**
     * edits miles returns true if edit to miles was successful 
     * @param miles the new miles value
     * @return true if edit to sleep was successful
     */
    public boolean editMiles(double miles){
        try{this.miles=miles;}
        catch(Exception e){
            return false;
        }
        return true;
    }
    /**
     * edits sleep returns true if edit to sleep was successful
     * @param sleep the new sleep value
     * @return true if edit to sleep was successful
     */
    public boolean editSleep(double sleep){
        try{this.sleep=sleep;}
        catch(Exception e){
            return false;
        }
        return true;
    }
    /**
     * edits the notes of the entry
     * @param notes the new notes of the entry
     */
    public void editNotes(String notes){this.notes=notes;}
    /**
     * Edits the time of the entry. Spak maybe just using 3 parameters instead 
     * of a string, we can use drop down bars in the applet
     */
    public boolean editTime(String time){
        if(time.length()<5){
            try{
                int firstColon = time.indexOf(':');
                minutes = Integer.parseInt(time.substring(0,firstColon));
                seconds = Integer.parseInt(time.substring(firstColon+1));
            }
            catch(Exception e){
                return false;
            }
        }
        try{
            int firstColon = time.indexOf(':');
            hours = Integer.parseInt(time.substring(0,firstColon));
            int secondColon = time.indexOf(':', firstColon+1);
            minutes = Integer.parseInt(time.substring(firstColon+1, secondColon));
            seconds = Integer.parseInt(time.substring(secondColon+1));
        }
        catch(Exception e){
            return false;
        }
        return true;
    }
    /**
     * edits the time of the entry using ints representing hours minutes and seconds
     * @param hours new Hours
     * @param minutes new Minutes
     * @param seconds new Seconds
     */
    public void editTime(int hours, int minutes, int seconds){
        this.hours=hours;
        this.minutes=minutes;
        this.seconds=seconds;
    }
    /**
     * Return miles.
     * @return miles
     */
    public double getMiles(){return miles;}
    /**
     * return the amount of sleep recorded in the entry
     * @return the amount of sleep recorded in the entry
     */
    public double getSleep(){return sleep;}
    /**
     * return the notes recorded in the entry
     * @return the notes recorded in the entry
     */
    public String getNotes(){return notes;}
    /**
     * return the hours in the entry
     * @return the hours in the entry
     */
    public int getHours(){return hours;}
    /**
     * return the minutes in the entry
     * @return the minutes in the entry
     */
    public int getMinutes(){return minutes;}
    /**
     * return the seconds in the entry
     * @return the seconds in the entry
     */
    public int getSeconds(){return seconds;}
    /**
     * return the raw input that was entered into the entry
     * @return the raw input that was entered into the entry
     */
    public String[] getRawInput(){return this.inputs;}
}
