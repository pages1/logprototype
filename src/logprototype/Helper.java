/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Reilly Noonan Grant
 * Helper class to contain all of the static methods to be used throughout 
 * the project
 */
public class Helper {
    /**
     * Returns the path of the file that contains the data for this application
     * @return the path of the file that contains the data for this application
     */
    public static String dataLocation(){
        String theory= dataLocHelp().getAbsolutePath();
        return theory;
    }
    /**
     * returns the file that contains the data for this application.
     * @return the file that contains the data for this application.
     */
    private static File dataLocHelp(){
        for(File file:File.listRoots()){
            for(File f:file.listFiles()){
                if(f.getName().equals("LogProtoTypeData")){
                    return f;
                }
            }
        }
        return null;
    }
    /**
     * returns true if the Account already exists, false other wise.
     * @param UserName the username to check
     * @param password the password to check
     * @return returns true if the Account already exists, false other wise.
     */
    public static Boolean entryExists(String UserName,String password){
        File saveLocation=new File(dataLocation()+"\\UserInfo\\RunnerInfo");//}
        if(!saveLocation.exists()){
            return entryExistsHelper(UserName,password);
        }
        File userFile = new File(saveLocation.getAbsolutePath()+"\\"+UserName+".txt");
        if(!userFile.exists()){
            return entryExistsHelper(UserName,password);
        }
        Scanner reader = null;
        try {
            reader = new Scanner(userFile);
        } catch (FileNotFoundException ex) {}
        String storedPassword= reader.nextLine();
        if(password.equals(storedPassword)){return true;} else {
            return entryExistsHelper(UserName,password);
        }
    }
    /**
     * Helps the Entry exists method by checking the coaches data
     * @param UserName the username to check
     * @param password the pass word to check
     * @return true if the Entry exists in the coaches data, false otherwise.
     */
    private static Boolean entryExistsHelper(String UserName,String password){
        File saveLocation=new File(dataLocation()+"\\UserInfo\\CoachInfo");//}
        if(!saveLocation.exists()){
            return false;
        }
        File userFile = new File(saveLocation.getAbsolutePath()+"\\"+UserName+".txt");
        if(!userFile.exists()){
            return false;
        }
        Scanner reader = null;
        try {
            reader = new Scanner(userFile);
        } catch (FileNotFoundException ex) {}
        if(!password.equals(reader.nextLine())){return false;}
        return true;
    }
    
    public static Boolean isRunner(String UserName,String password){
        return !entryExistsHelper(UserName,password);
    }
}
