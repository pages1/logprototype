/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

/**
 * a prototype in java of the web based application that me and 
 * Aaron Spak plan to build.
 * @author Reilly Noonan Grant
 * Runs the GUI for the application.
 */
public class JFrameLogRunner {
    
    private static LoginPage login=new LoginPage();
    
    public static void main(String[] args){
        login.setVisible(true);
    }
    
}
