/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.NavigableSet;
import java.util.Scanner;
import java.util.TreeMap;


/**
 *
 * @author Reilly Nooan Grant & Aaron Spak
 * Creating a prototype in java of the web based application that me and 
 * Aaron Spak plan to build. This holds the entries and does the calculations on them
 */
public class LogBook {
    private TreeMap<Date,Entry> entryMap; 
    private String user;
    private String teamName;
    private Scanner reader;
    private File master;
    public LogBook(String user,String teamName){
        this.user=user;
        this.teamName=teamName;
        startFiles();
    }
    
    /**
     * Does all the work with files that needs to be done with the logbook
     */
    private void startFiles(){
        master = new File(Helper.dataLocation()+"\\"+teamName+"\\"+user);
        if(!master.exists()){
            master.mkdirs();
        }
        for(File f:master.listFiles()){
            String theory =f.getName();
            int month,day,year;
            month=Integer.parseInt(theory.substring(0,theory.indexOf("_")));
            theory=theory.substring(theory.indexOf("_")+1);
            day=Integer.parseInt(theory.substring(0,theory.indexOf("_")));
            theory=theory.substring(theory.indexOf("_")+1);
            year=Integer.parseInt(theory.substring(0,theory.indexOf(".")));
            try {
                entryMap.put(new Date(month,day,year), new Entry(f));
            } catch (FileNotFoundException ex) {/*ex.printStackTrace();*/}
        }
    }
    
    /**
     * Adds an Entry with the given inputs to the LogBook and saves it's 
     * information under a given date
     * @param date the date to add the logbook under
     * @param inputs the inputs of the entry
     */
    public void addEntry(Date date,Entry entry) throws IOException{
        entryMap.put(date, entry);
        File enter= new File(master.getAbsolutePath()+"\\"+date+".txt");
        if(!enter.exists()){
            enter.createNewFile();
        }
        PrintWriter writer = new PrintWriter(enter);
        for(String theory:entry.getRawInput()){
            writer.println(theory);
        }
    }
   /**
     * Returns the Entry under the given date
     * @param date the date to find the entry of
     * @return entry associated with the given date
     */
    public Entry getEntry(Date date){
        return entryMap.get(date);
    }
    private ArrayList<Date> getDays(){
        ArrayList<Date> days= new ArrayList<Date>();
        for(Date d: entryMap.navigableKeySet()){
            days.add(d);
        }
        return days;
    }
    public int getConsecutiveDays(){
        int d=0;
        for(int i=0; i<getDays().size(); i++){
            if(getDays().get(i-1).getNumDaysBetween(getDays().get(i))==1){
                d++;
            }
            else{
                d=0;
            }
        }
        return d;
    }
    public int getTotalMiles(){
        int m=0;
        for(Entry e:entryMap.values()){
            m+=e.getMiles();
        }
        
        return m;
    }
}
