/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

/**
 *@author Reilly Nooan Grant & Aaron Spak
 * Creating a prototype in java of the web based application that me and 
 * Aaron Spak plan to build. This is one Users that will represent a
 * runner who will contains a logbook in 
 * addition to a user name and password
 */
public class Runner extends User{
    private LogBook log;
    private String teamName;
    
    public Runner(String userName, String password, String teamName){
        super(userName,password,teamName);
        log = new LogBook(userName,teamName);
    }
    /**
     * returns the logBook of the runner
     * @return the logBook of the runner
     */
    public LogBook getLogBook(){
        return log;
    }
    
}
