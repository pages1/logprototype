/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Reilly Noonan Grant
 * Creating a prototype in java of the web based application that me and 
 * Aaron Spak plan to build. This it a Team Object to hold references to all of
 * of the runners on the coaches team.
 */
public class Team {
    private ArrayList<Runner> runners;
    private String teamName;
    
    public Team(String teamName){
        runners = new ArrayList<>();
        this.teamName=teamName;
    }
    /**
     * returns an int representing the size of the team 
     * @return an int representing the size of the team
     */
    public ArrayList<Runner> getRunners(){return runners;}
    /**
     * helper method that helps with file loading and saving
     */
    private void startFiles(){
        File master = new File(Helper.dataLocation()+"\\UserInfo\\RunnerInfo");
        if(!master.exists()){
            master.mkdirs();
        }
        
        for(File f:master.listFiles()){
            String runnerName=f.getName();
            Scanner reader = null;
            try {
                reader = new Scanner(f);
            } catch (FileNotFoundException ex) {}
            String runnerPassword=reader.nextLine();
            String runnerTeamName=reader.nextLine();
            if(runnerTeamName.equals(teamName)){
                runners.add(new Runner(runnerName,runnerPassword,runnerTeamName));
            }
        }
    }
    
}
