/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logprototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Reilly Nooan Grant & Aaron Spak
 * Creating a prototype in java of the web based application that me and 
 * Aaron Spak plan to build. This is the User class of this prototype to help
 * with Users and file storage
 */
public abstract class User {
    private String password;
    private String userName;
    private String teamName;
    
    /**
     * Constructs a User with the given username and Password
     * @param password the password of the user
     * @param userName the username of the user
     */
    public User( String userName,String password,String teamName){
        this.password=password;
        this.userName=userName;
        this.teamName=teamName;
        try {
            saveInfo();
        } catch (Exception ex) {}
    }
    
    /**
     * Saves the username and password information to a file.
     * to be used in constructor
     */
    private void saveInfo() throws FileNotFoundException, IOException{
        File saveLocation;
        if(this instanceof Coach){
            saveLocation=new File(Helper.dataLocation()+"\\UserInfo\\CoachInfo");
        }else{saveLocation=new File(Helper.dataLocation()+"\\UserInfo\\RunnerInfo");}
        if(!saveLocation.exists()){
            saveLocation.mkdirs();
        }
        File userFile = new File(saveLocation.getAbsolutePath()+"\\"+userName+".txt");
        if(!userFile.exists()){
            userFile.createNewFile();
            PrintWriter writer= new PrintWriter(userFile);
            writer.println(password);
            writer.println(teamName);
            writer.close();
        }
    }
    
    /**
     * sets the UserName to the given string
     * @param uN 
     */
    public void setUserName(String uN){
        userName=uN;
    }
    
    /**
     * Sets the password to the given string
     * @param p the given string to set the password to
     */
    public void setPassword(String p){
        password = p;
    }
    
    /**
     * return the users user name
     * @return the users user name 
     */
    public String getUserName(){
        return userName;
    }
    
    /**
     * returns the users password
     * @return the users password
     */
    public String getPassword(){
        return password;
    }
    
    public String getTeamName(){
        return teamName;
    }
    
}
